import os, gi
from gi.repository import GLib
from gi.repository import GObject
from gi.repository import Gio
gi.require_version('Gdk', '4.0')
from gi.repository import Gdk
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk
gi.require_version('GtkSource', '5')
from gi.repository import GtkSource

class Editor_Menu:
  def __init__(self, _window, _config, menu_bar, file_class):
    self.window = _window
    self.config = _config
    self.menu_bar = menu_bar
    self.file_class = file_class
    self.path_list = []
    self.current_page = 0
    self.view = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
    self.view.set_vexpand(True)
    self.notebook = Gtk.Notebook()
    self.notebook.set_scrollable(True)
    self.notebook.set_vexpand(True)
    self.notebook.popup_enable()
    self.notebook.connect("switch-page", self.switch_page)
    self.view.append(self.notebook)
    self.file_dialog = Gtk.FileDialog.new()
    self.insert_label = Gtk.Label.new("")
    self.view.append(self.insert_label)
    self.insert_label.set_visible(False)

    _action = Gio.SimpleAction.new("add-new-file", GLib.VariantType.new("s"))
    _action.connect("activate", self.add_from_fileselect)
    self.window.add_action(_action)
    _action = Gio.SimpleAction.new("save", None)
    _action.connect("activate", self.save_file)
    self.window.add_action(_action)
    _action = Gio.SimpleAction.new("save-as", None)
    _action.connect("activate", self.save_file_as_open)
    self.window.add_action(_action)
    self.menu_bar.append("Save as", "win.save-as")
    _action = Gio.SimpleAction.new("open", None)
    _action.connect("activate", self.open_file_open)
    self.window.add_action(_action)
    self.menu_bar.append("Open", "win.open")
    _action = Gio.SimpleAction.new("new", None)
    _action.connect("activate", self.new_tab)
    self.window.add_action(_action)
    self.menu_bar.append("New", "win.new")
    _action = Gio.SimpleAction.new("folder-root", None)
    _action.connect("activate", self.file_menu)
    self.window.add_action(_action)
    _action = Gio.SimpleAction.new("folder-home", None)
    _action.connect("activate", self.file_menu)
    self.window.add_action(_action)

    file_menu = Gio.Menu.new()
    self.menu_bar.append_submenu("File", file_menu)
    folder_menu = Gio.Menu.new()
    folder_menu.append("Home", "win.folder-home")
    folder_menu.append("/", "win.folder-root")
    file_menu.append_submenu("Folder", folder_menu)
    _action = Gio.SimpleAction.new_stateful('dot-files', None,
      GLib.Variant.new_boolean(self.config.getboolean("options", "dot-files")))
    _action.connect("change-state", self.file_menu)
    self.window.add_action(_action)
    file_menu.append("Dot files", "win.dot-files")

    view_menu = Gio.Menu.new()
    self.menu_bar.append_submenu("View", view_menu)
    _action = Gio.SimpleAction.new_stateful('line-num', None, GLib.Variant.new_boolean(self.config.getboolean("options", "line-numbers")))
    _action.connect("change-state", self.view_menu)
    self.window.add_action(_action)
    view_menu.append("Line numbers", "win.line-num")
    _action = Gio.SimpleAction.new_stateful('insert', None, GLib.Variant.new_boolean(self.config.getboolean("options", "insert")))
    _action.connect("change-state", self.view_menu)
    self.window.add_action(_action)
    view_menu.append("Insert", "win.insert")
    _action = Gio.SimpleAction.new_stateful('auto-indent', None, GLib.Variant.new_boolean(self.config.getboolean("options", "auto-indent")))
    _action.connect("change-state", self.view_menu)
    self.window.add_action(_action)
    view_menu.append("Auto indent", "win.auto-indent")
    _action = Gio.SimpleAction.new_stateful('high-curr', None, GLib.Variant.new_boolean(self.config.getboolean("options", "highlight-current")))
    _action.connect("change-state", self.view_menu)
    self.window.add_action(_action)
    view_menu.append("Highlight current", "win.high-curr")
    _action = Gio.SimpleAction.new_stateful('high-synt', None, GLib.Variant.new_boolean(self.config.getboolean("options", "highlight-syntax")))
    _action.connect("change-state", self.view_menu)
    self.window.add_action(_action)
    view_menu.append("Highlight syntax", "win.high-synt")
    _action = Gio.SimpleAction.new_stateful('fileselect', None, GLib.Variant.new_boolean(self.config.getboolean("options", "show-file")))
    _action.connect("change-state", self.view_menu)
    self.window.add_action(_action)
    view_menu.append("Fileselect", "win.fileselect")

    self.manager_lang = GtkSource.LanguageManager.get_default()
    self.manager_style = GtkSource.StyleSchemeManager.get_default()
    self.manager_style.prepend_search_path(os.path.abspath(os.path.dirname(__file__)) + "/styles/")
    self.source_file = GtkSource.File.new()

  def buffer_modified(self, _buffer, _2=None):
    if not self.load_mode:
      _bool = _buffer.get_modified()
      _label = self.notebook.get_tab_label(self.notebook.get_nth_page(
        self.notebook.get_current_page())).get_first_child()
      _text = _label.get_text().lstrip("* ")
      if _bool:
        _label.set_text("* " + _text)
      else:
        _label.set_text(_text)

  def add_from_fileselect(self, _action, _var):
    self.add_new_tab(_var.get_string())

  def add_new_tab(self, _path):
    if _path in self.path_list:
      for i in range(self.notebook.get_n_pages()):
        _page = self.notebook.get_nth_page(i)
        _path0 = self.notebook.get_menu_label_text(_page)
        if _path == _path0:
          self.notebook.set_current_page(i)
          break
      return
    _buffer = GtkSource.Buffer.new()
    if _path:
      _name = os.path.basename(_path)
      _lang = self.manager_lang.guess_language(_path)
      self.source_file.set_location(Gio.File.new_for_path(_path))
      _loader = GtkSource.FileLoader.new(_buffer, self.source_file)
      _loader.load_async(0, None, None, None, None, None)
      _buffer.set_language(_lang)
    else:
      _name = "New"
    _style = self.manager_style.get_scheme(self.config.get("options", "style"))
    _buffer.set_style_scheme(_style)
    _textview = GtkSource.View.new_with_buffer(_buffer)
    _hbox = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 5)
    _label = Gtk.Label.new(_name)
    _label.set_hexpand(True)
    _hbox.append(_label)
    _button = Gtk.Button.new_with_label("x")
    _hbox.append(_button)
    _scrollw = Gtk.ScrolledWindow.new()
    _scrollw.set_child(_textview)
    self.notebook.append_page(_scrollw, _hbox)
    _button.connect("clicked", self.remove_tab, _scrollw)
    self.notebook.set_current_page(-1)
    if _path:
      self.path_list.append(_path)
      self.notebook.set_menu_label_text(_scrollw, _path)
    self.notebook.set_tab_reorderable(_scrollw, True)
    self.set_load_mode(True)
    _buffer.connect("cursor-moved", self.cursor_moved)
    _buffer.connect("modified-changed", self.buffer_modified)
    _textview.set_monospace(True)
    _textview.set_margin_bottom(5)
    self.switch_page(None, None, -1)
    self.notebook.set_show_tabs(self.notebook.get_n_pages() - 1)
    GLib.timeout_add(50, self.set_load_mode, False)

  def set_load_mode(self, _mode):
    self.load_mode = _mode

  def new_tab(self, _1=None, _2=None):
    self.add_new_tab(None)

  def remove_tab(self, _button, _page):
    if self.notebook.get_n_pages() > 1:
      _path = self.notebook.get_menu_label_text(_page)
      self.notebook.remove_page(self.notebook.page_num(_page))
      self.notebook.set_show_tabs(self.notebook.get_n_pages() - 1)
      if _path:
        self.path_list.remove(_path)

  def save_file(self, _1, _2):
    _page = self.notebook.get_nth_page(self.notebook.get_current_page())
    _path = self.notebook.get_menu_label_text(_page)
    if _path:
      _buffer = _page.get_child().get_buffer()
      self.source_file.set_location(Gio.File.new_for_path(_path))
      _saver = GtkSource.FileSaver.new(_buffer, self.source_file)
      _saver.save_async(0, None, None, None, None, None)
      _buffer.set_modified(False)

  def save_file_as_open(self, _1=None, _2=None, _3=None):
    self.file_dialog.save(self.window, None, self.save_file_as_finish)

  def save_file_as_finish(self, _dialog, _task, _3=None):
    if not _task.had_error():
      _gfile = _dialog.save_finish(_task)
      _path = _gfile.get_path()
      if not _path in self.path_list:
        _page = self.notebook.get_nth_page(self.notebook.get_current_page())
        _path_old = self.notebook.get_menu_label_text(_page)
        _textview = _page.get_child()
        _buffer = _textview.get_buffer()
        self.source_file.set_location(_gfile)
        _saver = GtkSource.FileSaver.new(_buffer, self.source_file)
        _saver.save_async(0, None, None, None, None, None)
        if _path_old:
          self.path_list.remove(_path_old)
        self.path_list.append(_path)
        self.notebook.set_menu_label_text(_page, _path)
        _hbox = self.notebook.get_tab_label(_page)
        _label = _hbox.get_first_child()
        _label.set_text(os.path.basename(_path))
        _buffer.set_modified(False)
        self.window.set_title("Magnum - " + _path)

  def open_file_open(self, _1=None, _2=None, _3=None):
    self.file_dialog.open(self.window, None, self.open_file_finish)

  def open_file_finish(self, _dialog, _task, _3=None):
    if not _task.had_error():
      _gfile = _dialog.open_finish(_task)
      self.add_new_tab(_gfile.get_path())

  def switch_page(self, _1, _2, _index):
    _page = self.notebook.get_nth_page(_index)
    _textview = _page.get_child()
    _textview.set_show_line_numbers(self.window.lookup_action("line-num").get_state())
    _textview.set_auto_indent(self.window.lookup_action("auto-indent").get_state())
    _textview.set_highlight_current_line(self.window.lookup_action("high-curr").get_state())
    _textview.get_buffer().set_highlight_syntax(self.window.lookup_action("high-synt").get_state())
    self.cursor_moved(_textview.get_buffer())
    _path = self.notebook.get_menu_label_text(_page)
    if not _path:
      _path = "New"
    self.window.set_title("Magnum - " + _path)

  def file_menu(self, _action, _state):
    _name = _action.get_name()
    if _name == "dot-files":
      _action.set_state(GLib.Variant.new_boolean(_state))
      self.file_class.fileselect_reset(self.config.get("options", "startup-dir"))
    elif _name == "folder-home":
      self.file_class.fileselect_reset("Home")
    elif _name == "folder-root":
      self.file_class.fileselect_reset("/")

  def view_menu(self, _action, _state):
    _name = _action.get_name()
    _action.set_state(GLib.Variant.new_boolean(_state))
    if _name == "line-num" or _name == "auto-indent" or _name == "high-curr" or _name == "high-synt":
      self.switch_page(None, None, self.notebook.get_current_page())
    elif _name == "insert":
      self.insert_label.set_visible(_state)
    elif _name == "fileselect":
      self.file_class.view.set_visible(_state)

  def cursor_moved(self, _buffer, _2=None):
    _iter = _buffer.get_iter_at_mark(_buffer.get_insert())
    self.insert_label.set_text("Ln: {0}, Col: {1}".format(_iter.get_line() + 1,
      _iter.get_line_offset()))

