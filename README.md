# Magnum - text editor

## Dependencies
- Python
- GTK
- GTK-Sourceview

## Build Dependencies
- python-build
- python-setuptools
- python-pip

## Install
```
$ python -m build
# pip install dist/*.whl
```
Copy the files in the data-files directory to their locations.
```
# cp data-files/magnum /usr/bin/
# cp data-files/magnum.desktop /usr/share/applications/
# cp data-files/magnum.svg /usr/share/icons/
```

## Run from source
```
$ python app-run
```

