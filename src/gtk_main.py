import sys, os, gi, configparser, shutil
gi.require_version('Gdk', '4.0')
from gi.repository import Gdk
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk
from gi.repository import Gio
from gi.repository import GLib
from . import fileselect
from . import editor_menu

def app_init_src(file_path):
  global configpath
  configpath = os.getcwd() + "/src/data/"
  application = Gtk.Application().new(None, 0)
  application.connect("activate", GTK_Main, file_path)
  application.run()

def app_init(file_path):
  global configpath
  configpath = os.environ['HOME'] + "/.config/magnum/"
  if not os.path.exists(configpath):
    os.makedirs(configpath + "styles")
  if not os.path.exists(configpath + "settings.conf"):
    for i in sys.path:
      _path = i + "/magnum/data/settings.conf"
      if os.path.exists(_path):
        shutil.copy(_path, configpath)
        break
  application = Gtk.Application().new(None, 0)
  application.connect("activate", GTK_Main, file_path)
  application.run()

class GTK_Main:
  def __init__(self, _app, file_path):
    self.config = configparser.ConfigParser(allow_no_value=True)
    self.config.read([configpath + "settings.conf"])
    self.application = _app
    self.window = Gtk.ApplicationWindow.new(self.application)
    self.window.set_default_size(self.config.getint("options", "window-width"),
      self.config.getint("options", "window-height"))
    self.menu_bar = Gio.Menu.new()
    self.headerbar = Gtk.HeaderBar.new()
    self.window.set_titlebar(self.headerbar)
    self.vbox = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
    self.window.set_child(self.vbox)
    self.window.connect("destroy", self.app_quit)
    self.window.connect("close-request", self.app_quit)
    self.window.set_title("Magnum")
    _action = Gio.SimpleAction.new("quit", None)
    _action.connect("activate", self.app_quit)
    self.window.add_action(_action)

    _button = Gtk.Button.new_with_label("Open Menu")
    _button.set_action_name("app.open-menu")
    _button.set_icon_name("open-menu-symbolic")
    _button.set_margin_end(10)
    self.headerbar.pack_end(_button)

    self.popover_menu = Gtk.PopoverMenu.new_from_model(self.menu_bar)
    self.popover_menu.set_parent(_button)
    _action = Gio.SimpleAction.new("open-menu", None)
    _action.connect("activate", self.menu_open)
    self.application.add_action(_action)

    _button = Gtk.Button.new_with_label("New tab")
    _button.set_action_name("win.new")
    _button.set_icon_name("tab-new")
    _button.set_margin_end(10)
    self.headerbar.pack_end(_button)

    _button = Gtk.Button.new_with_label("Save")
    _button.set_margin_end(10)
    _button.set_action_name("win.save")
    self.headerbar.pack_end(_button)

    self.hpaned = Gtk.Paned().new(Gtk.Orientation.HORIZONTAL)
    self.hpaned.set_vexpand(True)
    self.file_class = fileselect.Fileselect(self.window)
    self.menu_class = editor_menu.Editor_Menu(self.window, self.config, self.menu_bar, self.file_class)
    self.file_class.view.set_size_request(self.config.getint("options", "file-width"), -1)
    self.menu_class.view.set_size_request(self.config.getint("options", "window-width") - self.config.getint("options", "file-width"), -1)
    self.hpaned.set_start_child(self.file_class.view)
    self.hpaned.set_end_child(self.menu_class.view)

    self.vbox.append(self.hpaned)
    self.window.set_visible(True)
    self.file_class.view.set_visible(self.config.getboolean("options", "show-file"))
    self.file_class.fileselect_reset(self.config.get("options", "startup-dir"))
    self.application.set_accels_for_action("app.open-menu", ["<Ctrl>m"])
    self.application.set_accels_for_action("win.save", ["<Ctrl>s"])
    self.application.set_accels_for_action("win.save-as", ["<Shift><Ctrl>s"])
    self.application.set_accels_for_action("win.open", ["<Ctrl>o"])

    if file_path:
      if not file_path.startswith("/"):
        file_path = "{0}/{1}".format(os.getcwd(), file_path)
    self.menu_class.add_new_tab(file_path)

  def menu_open(self, _1, _2):
    self.popover_menu.popup()

  def app_quit(self, _1, _2=None):
    self.application.quit()

