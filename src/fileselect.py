import os, gi, shutil
from gi.repository import Gio
from gi.repository import GLib
gi.require_version('Gdk', '4.0')
from gi.repository import Gdk
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk

class Fileselect:
  def __init__(self, _window):
    self.window = _window
    self.view = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
    self.scrollview = Gtk.ScrolledWindow.new()
    self.scrollview.set_vexpand(True)
    self.entry = Gtk.Entry.new()
    self.entry.set_editable(False)
    self.entry.unset_state_flags(Gtk.StateFlags.FOCUSED)
    self.entry.set_hexpand(True)
    self.view.append(self.entry)
    self.view.append(self.scrollview)
    self.view.set_margin_start(0)
    self.file_name = None
    self.file_path = None
    self.stringlist = Gtk.StringList.new([])
    self.treelist = Gtk.TreeListModel.new(self.stringlist, False, False, self.create_model, None)
    self.selection = Gtk.SingleSelection.new(self.treelist)
    self.factory = Gtk.SignalListItemFactory.new()
    self.factory.connect("setup", self.factory_setup)
    self.factory.connect("bind", self.factory_bind)
    self.listview = Gtk.ListView.new(self.selection, self.factory)
    self.listview.connect("activate", self.listview_callback)
    self.scrollview.set_child(self.listview)
    self.listview.set_margin_bottom(8)
    _event = Gtk.GestureClick.new()
    _event.connect("pressed", self.button_press)
    _event.connect("released", self.button_release)
    _event.set_button(0)
    self.listview.add_controller(_event)
    _event = Gtk.EventControllerKey.new()
    _event.connect("key-released", self.key_release)
    self.listview.add_controller(_event)
    self.menu = Gio.Menu.new()
    _action = Gio.SimpleAction.new("file-delete", None)
    _action.connect("activate", self.options_menu)
    self.window.add_action(_action)
    self.menu.append("Delete", "win.file-delete")
    self.popover = Gtk.PopoverMenu.new_from_model(self.menu)
    self.popover.set_parent(self.listview)

  def create_model(self, _item, _1):
    _path = _item.get_string()
    if os.path.isdir(_path):
      _stringlist = Gtk.StringList.new(self.get_dir_list(_path))
    else:
      _stringlist = None
    return _stringlist

  def fileselect_reset(self, _path):
    if _path.lower() == "home":
      _path = os.environ["HOME"]
    self.stringlist.splice(0, self.stringlist.get_n_items(), self.get_dir_list(_path))
    self.set_file_path()

  def get_dir_list(self, dir_path):
    dir_list = []
    file_list = []
    _dotfiles = self.window.lookup_action("dot-files").get_state()
    _list = os.listdir(dir_path)

    for _name in _list:
      if dir_path == "/":
        _path = "/" + _name
      else:
        _path = dir_path + "/" + _name
      if _dotfiles:
        if os.path.isdir(_path):
          dir_list.append(_path)
        else:
          file_list.append(_path)
      else:
        if not _name.startswith("."):
          if os.path.isdir(_path):
            dir_list.append(_path)
          else:
            file_list.append(_path)

    dir_list.sort()
    file_list.sort()
    return dir_list + file_list

  def factory_setup(self, _factory, _item):
    _hbox = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 8)
    _image = Gtk.Image.new_from_icon_name(None)
    _hbox.append(_image)
    _label = Gtk.Label.new("")
    _hbox.append(_label)
    _expander = Gtk.TreeExpander.new()
    _expander.set_child(_hbox)
    _item.set_child(_expander)

  def factory_bind(self, _factory, _item):
    _path = _item.get_item().get_item().get_string()
    _hbox = _item.get_child().get_child()
    _image = _hbox.get_first_child()
    _label = _hbox.get_last_child()
    if os.path.isdir(_path):
      _image.set_from_icon_name("folder-symbolic")
    else:
      _image.set_from_icon_name("document-open-symbolic")
    _item.get_child().set_list_row(_item.get_item())
    _name = os.path.basename(_path)
    _label.set_text(_name)

  def listview_callback(self, _view, _pos):
    _row = self.treelist.get_row(_pos)
    _path = _row.get_item().get_string()
    if os.path.isdir(_path):
      _row.set_expanded(not _row.get_expanded())
    else:
      self.file_path = _path
      self.file_name = os.path.basename(_path)
      if os.path.isfile(_path):
        self.window.lookup_action("add-new-file").activate(GLib.Variant.new_string(_path))

  def button_press(self, gesture, npress, _x=None, _y=None, _2=None):
    if gesture.get_current_button() == 3:
      self.popover.popup()
      _rect = Gdk.Rectangle()
      _rect.x = _x
      _rect.y = _y
      self.popover.set_property("pointing-to", _rect)
    else:
      GLib.timeout_add(200, self.set_file_path)

  def button_release(self, gesture, npress, _0=None, _1=None, _2=None):
    GLib.timeout_add(200, self.set_file_path)

  def key_release(self, w, event, _key, _1):
    self.set_file_path()

  def set_file_path(self):
    _pos = self.selection.get_selected()
    self.file_path = self.treelist.get_row(_pos).get_item().get_string()
    self.file_name = os.path.basename(self.file_path)
    self.entry.set_text(self.file_path)

  def get_index(self, _path):
    _index = None
    for i in range(self.treelist.get_n_items()):
      if _path == self.treelist.get_row(i).get_item().get_string():
        _index = i
        break
    return _index

  def options_menu(self, _action, _window):
    _name = _action.get_name()
    if _name == "file-delete":
      if os.path.isdir(self.file_path):
        shutil.rmtree(self.file_path)
      else:
        os.remove(self.file_path)
      _parent = os.path.dirname(self.file_path)
      _pos = self.get_index(_parent)
      if _pos != None:
        self.listview_callback(None, _pos)
        self.selection.set_selected(_pos)
        self.treelist.get_row(_pos).set_expanded(True)
        self.set_file_path()
      else:
        self.fileselect_reset("home")
        self.selection.set_selected(0)

